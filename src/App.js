import React, { useState} from 'react';
import './App.css';

const images=[
  {name:"智能呼叫中心",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/33ebf503.png",desc:"提供呼叫中心专有识别模型，实时准确的将语音识别为文字。可用于语音IVR、语音机器人、客服对话辅助、语音质检等场景，大幅度降低人工成本"},
  {name:"智能工业制造",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/e44b4ce7.png",desc:"面向工业制造领域的全数据分析平台，结合百度质检云质量数据从产品质量数据的分析定位缺陷的成因，反哺工艺优化促进产品迭代，实现AI智能数据的应用闭环"},
  {name:"数据仓储",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/8f20ab8a.png",desc:"数据仓储（Data Warehousing）是企业为了分析数据进而获取洞察力的努力，是商务智能的主要环节。在大数据时代，百度智能云提供了云端的数据仓储解决方案，为企业搭建现代数据仓库提供指南"},
  {name:"教育行业",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/7fae2a84.png",desc:"依托稳定的云计算基础服务，借助“百度文库”的生态内容，为您构建百度独有的“基础云技术+教育云平台+教育大数据”解决方案，推进教育行业的数字化和智能化，极大地促进行业的转型升级"},
  {name:"网站及部署",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/c182f430.png",desc:"结合百度生态专属优势，打通网站全生命周期需求，从域名、建站、备案、选型、部署、测试到运维、推广、变现，想你所需，做最懂站长的网站云服务"},
  {name:"视频智能应用",url:"https://bce.bdstatic.com/bce-portal/static/assets/assets/8297736c.png",desc:"基于百度业界领先的人工智能技术、大数据技术，帮助传统媒体、短视频类客户，实现对视频内容的理解，并基于此构建视频推荐、视频搜索和视频广告系统。提升用户活跃和留存，增大广告变现收益"},
];

const SwiperBaidu = (props) => {
  const [index, setCount] = useState(1);

  const turn = (step)=>{
    let i =index+step;
    if(i>=props.images.length){
      i=0;
    }
    if(i<0){
      i = props.images.length-1;
    }
    setCount(i)
  };

  let stylees={
    ulStyle:{
      left: "495px",
      transform:"translateX("+(-index*536)+"px)",
    },
  }


  let pre,next;
  if(index > 0){
    pre = <div className="pre" onClick={()=>turn(-1)}>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABABAMAAABYR2ztAAAAHlBMVEVHcExmcopmc4t2dpdlcopmcotmcopqcY5mdYplcYnLHQOMAAAACXRSTlMA40sLoX3AJCNEwqCQAAAAeklEQVRIx2NgGAWjAC9QFCKgQHKmAiEF4vgVaM6caIBXAevMmcn4jfCcOQW/ArOZMwPwKmCWnDkJvxGNMwn4lGnm4PSp4kx0MAk9htDBRBIVELSC1Ogg5E2CAUUoqIGRJYHXgEIC0U0wwQy8H4HxJkFh5iWY/UfBCAYAzs5Q48MpPuQAAAAASUVORK5CYII=" alt="" />>
          </div>;
  }
  if(index < props.images.length-1){
    next = <div className="next" onClick={()=>turn(1)}>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABABAMAAABYR2ztAAAAHlBMVEVHcExmcopmc4t2dpdlcopmcotmcopqcY5mdYplcYnLHQOMAAAACXRSTlMA40sLoX3AJCNEwqCQAAAAeklEQVRIx2NgGAWjAC9QFCKgQHKmAiEF4vgVaM6caIBXAevMmcn4jfCcOQW/ArOZMwPwKmCWnDkJvxGNMwn4lGnm4PSp4kx0MAk9htDBRBIVELSC1Ogg5E2CAUUoqIGRJYHXgEIC0U0wwQy8H4HxJkFh5iWY/UfBCAYAzs5Q48MpPuQAAAAASUVORK5CYII=" alt="" />>
          </div>;
  }
  return (
      <div className="App">
        <div className="swiper">
          <ul className="stabnavs">
            {
              props.images.map((item,i)=>(
                <li className={"stabnav " +(i === index?'active':'')} key={i} onClick={()=>turn(i-index)}>
                  <p>{item.name}</p>
                </li>
              ))
            }
          </ul>
          {/* <div className="stabnavline"><span style={stylees.tabnavlineStyle}></span></div> */}
          <div className="scontent">
                <ul style={stylees.ulStyle}>
                  {
                    props.images.map((item,i)=>(
                      <li className={"card "+(i === index ? 'active':'')} key={i}>
                        <div className="img-content">
                            <div className="mask"></div>
                            <img src={item.url} alt={item.name} />>
                        </div>
                        <div className="text-content">
                            <h3>{item.name}</h3>
                            <p className="desc">{item.desc}</p>
                            <a href="https://cloud.baidu.com/solution/bicc" data-track-category="解决方案/智能呼叫中心" data-track-name="查看详情">
                                <span>查看详情</span>
                            </a>
                        </div>
                    </li>
                    ))
                  }
                </ul>
                <div className="change">
                  {pre}
                  {next}
                </div>
            </div>
        
          <ul className="sdotnavs">
            {
              props.images.map((item,i)=>(
                <li className={"sdotnav " +(i === index?'active':'')} key={i} onClick={()=>turn(i-index)}>
                  <span className="line"></span>
              </li>
              ))
            }
          </ul>
      
        </div>
      </div>
  )
}

class App extends React.Component {
 
  render(){
    return (
     <div>
       <SwiperBaidu images={images}/>
     </div>
   );
  }
}

export default App;
